export class ScreenManager
	loadScreen: (screen) =>
		return unless screen
		@currentScreen\unload! if @currentScreen
		@currentScreen = screen
		@currentScreen.screenManager = self
		@currentScreen\load!

	draw: =>
		love.graphics.setColor 255, 255, 255 
		@currentScreen\draw! if @currentScreen
	update: (deltaTime) => @currentScreen\update deltaTime if @currentScreen
	keypressed: (key, isRepeat) => @currentScreen\keypressed key, isRepeat if @currentScreen
	mousepressed: (x, y, button) => @currentScreen\mousepressed x, y, button if @currentScreen