export class Background extends Sprite
	layers: {
		{name: "rocks2", speed: 0.8},
		{name: "rocks1", speed: 0.7},
		{name: "trees",  speed: 0.2},
		{name: "grass",  speed: 0}
	}

	offset: 0

	new: (width, height) =>
		for i, layer in ipairs @layers
			layer.texture = love.graphics.newImage "assets/" .. layer.name .. ".png"
			layer.texture\setFilter "nearest", "nearest"

		@width = @layers[1].texture\getWidth!
		@height = height

		love.graphics.setBackgroundColor 61, 203, 219

	draw: =>
		for i, layer in ipairs @layers
			for j = 0, 4
				love.graphics.draw layer.texture, j * layer.texture\getWidth! + @offset * layer.speed, @height - layer.texture\getHeight!

	move: (x) => @offset = x