export class ParticleEmitter
	type: ""
	particles: {}
	gravity: 0
	frictionX: 1
	frictionY: 1
	scaleAdd: 0
	spawnEveryFrame: false
	spawnDelay: 0
	spawnDelayCurrent: 0
	spawnCount: 0
	lifeTime: 1

	originX: 0
	originY: 0
	x: 0
	y: 0

	new: (x = 0, y = 0, type = "smoke", ...) =>
		@x = x
		@y = y
		@type = type
		args = {...}

		switch type
			when "smoke"
				@texture 	= love.graphics.newImage "assets/particles/smoke.png"
				@gravity 	= 0
				@frictionX 	= 0.99
				@frictionY 	= 0.99
				@scaleAdd 	= 0.05
				@spawnEveryFrame = true
				@spawnDelay = 0.01
				@spawnCount = args[1]
				@lifeTime = 1
			when "explosion"
				@texture = love.graphics.newImage "assets/particles/fire.png"
				@gravity = 10
				@frictionX = 0.99
				@frictionY = 1
				@scaleAdd = -0.05
				@spawnEveryFrame = false
				@spawnCount = 80
				@lifeTime = 0.1

		if @texture
			@texture\setFilter "nearest", "nearest"
			@originX = @texture\getWidth! / 2
			@originY = @texture\getHeight! / 2

		@spawnDelayCurrent = @spawnDelay
		@particles = {}

	start: (x, y) =>
		return if @spawnEveryFrame
		@particles = {}
		@x = x
		@y = y
		for i = 1, @spawnCount
			@addParticle!

	addParticle: =>
		particle = {
			x: 		@x,
			y:  	@y,
			sx: 	0,
			sy: 	0,
			scale: 	1,
			rotation: 0,
			lifeTime: @lifeTime,
			rotationSpeed: 0
		}

		switch @type
			when "smoke"
				particle.sx = math.random -10, 10
				particle.sy = math.random -20, 20
				particle.scale = 0.5
				particle.lifeTime *= math.random(80, 100) / 100
				particle.rotation = math.random(-100, 100) / 100
				particle.rotationSpeed = math.random(-100, 100) / 5000
			when "explosion"
				particle.sx = math.random -150, 150
				particle.sy = math.random -50, -300
				particle.scale = 3
				particle.rotation = math.random(-100, 100) / 100
				particle.rotationSpeed = math.random(-100, 100) / 1000
		table.insert @particles, particle

	update: (deltaTime) =>
		toRemove = {}
		for i, particle in ipairs @particles
			particle.x 	+= particle.sx * deltaTime
			particle.y 	+= particle.sy * deltaTime
			particle.sx *= @frictionX
			particle.sy *= @frictionY
			particle.sy += @gravity
			particle.scale += @scaleAdd
			particle.rotation += particle.rotationSpeed
			particle.lifeTime -= @lifeTime * deltaTime
			if particle.lifeTime <= 0
				table.insert toRemove, i

		for i, index in ipairs toRemove
			table.remove @particles, index

		if @spawnEveryFrame
		 	@spawnDelayCurrent -= deltaTime
		 	if @spawnDelayCurrent <= 0 
		 		@spawnDelayCurrent = @spawnDelay
		 		for i = 1, @spawnCount
		 			@addParticle!

	draw: =>
		love.graphics.setColor 255, 255, 255, 255
		for i, particle in ipairs @particles
			alpha = 255 * particle.lifeTime / @lifeTime
			love.graphics.setColor 255, 255, 255, alpha 
			if alpha > 0
		 		love.graphics.draw @texture, particle.x, particle.y, particle.rotation, particle.scale, particle.scale