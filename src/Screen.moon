export class Screen
	-- Fields
	name: "SCREEN_NAME"
	-- Methods
	load: => 
	unload: =>
	draw: =>
	update: (deltaTime) =>
	keypressed: (key, isRepeat) =>
	mousepressed: (x, y, button) =>