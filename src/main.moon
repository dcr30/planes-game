require "utils/NameGenerator"
require "ScreenManager"
require "Screen"
require "screens/GameScreen"
require "screens/MenuScreen"
local screenManager

love.load = ->
	print("Reading game config file")
	ConfigManager\ReadConfig!
	if ConfigManager.multiplayer.name == "random_name"
		ConfigManager.multiplayer.name = NameGenerator\GetRandomName!

	screenManager = ScreenManager!
	screenManager\loadScreen MenuScreen!

love.draw = ->
	screenManager\draw!

love.update = (deltaTime) ->
	screenManager\update deltaTime

love.keypressed = (key, isRepeat) ->
	screenManager\keypressed key, isRepeat

love.mousepressed = (x, y, button) ->
	screenManager\mousepressed x, y, button