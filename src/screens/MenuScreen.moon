export class MenuScreen extends Screen
	name: "Menu"
	buttons: {
		"CONNECT",
		"HOST"
	}

	-- to #{ConfigManager.multiplayer.host}:#{ConfigManager.multiplayer.port}
	load: =>
		-- debug
		@screenManager\loadScreen GameScreen true

	draw: =>
		love.graphics.setColor 255, 100, 0
		love.graphics.print ConfigManager.multiplayer.name, 100, 100
		mouseX = love.mouse.getX!
		mouseY = love.mouse.getY!
		for i, button in ipairs @buttons do
			offset = 100 + i * 20
			if mouseY > offset - 5 and mouseY < offset - 5 + 20 and mouseX > 90 and mouseX < 200
				love.graphics.setColor 255, 255, 255
			else
				love.graphics.setColor 100, 100, 100
			love.graphics.print button, 100, offset

	mousepressed: (x, y, button) =>
		buttonIndex = math.floor (y - 100 + 5) / 20
		return unless @buttons[buttonIndex]
		switch @buttons[buttonIndex]
			when "CONNECT"
				@screenManager\loadScreen GameScreen false, ConfigManager.multiplayer.host, ConfigManager.multiplayer.port
			when "HOST"
				@screenManager\loadScreen GameScreen true