require "utils/MathHelper"
require "utils/StringHelper"
require "utils/ConfigManager"
require "network/NetworkManager"
require "Camera"
require "ParticleEmitter"
require "Sprite"
require "Background"
require "Player"

export class GameScreen extends Screen
	name: "Game"
	syncDelay: 0 -- debug
	interpolationMultiplier: 4.4 -- 2.2
	new: (isHost, host, port) =>
		print("Game init")
		@camera = Camera!

		@background = Background @camera.width, @camera.height
		@worldWidth = @background.width * 4

		@player = Player!
		@player.x = 100
		@player.y = 100

		@remotePlayer = Player!

		@networkManager = NetworkManager!
		if isHost
			@networkManager\hostServer!
		else
			@networkManager\connect host, port
				
	draw: =>
		if @networkManager.isConnected
			love.graphics.print "Connected"

		@camera\set!
		@background\draw!
		if @networkManager.isConnected
			@remotePlayer\draw!
			if @networkManager.remoteName
				width = love.graphics.getFont!\getWidth @networkManager.remoteName
				rotationRad = @remotePlayer.rotation / 180 * math.pi
				love.graphics.print @networkManager.remoteName, @remotePlayer.x, @remotePlayer.y, rotationRad, 1, 1, width / 2, 40
		@player\draw!
		@camera\unset!

	updateInput: (deltaTime) =>
		if love.keyboard.isDown "right"
			@player\setRotation @player.rotation + @player.rotationSpeed * deltaTime
		elseif love.keyboard.isDown "left"
			@player\setRotation @player.rotation - @player.rotationSpeed * deltaTime

		if love.keyboard.isDown "up"
			@player\setPower @player.power + @player.powerSpeed * deltaTime
		elseif love.keyboard.isDown "down"
			@player\setPower @player.power - @player.powerSpeed * deltaTime

		
	update: (deltaTime) =>
		@updateInput deltaTime
		@networkManager\update @player.x, @player.y, @player.rotation

		-- Test
		-- if @syncDelay <= 0 
		-- 	@networkManager.remoteX = @player.x
		-- 	@networkManager.remoteY = @player.y
		-- 	@networkManager.remoteRotation = @player.rotation
		-- 	@syncDelay = 0.1
		-- else
		-- 	@syncDelay -= deltaTime

		if @networkManager.remoteX
			@remotePlayer.x += (@networkManager.remoteX - @remotePlayer.x) * @interpolationMultiplier * deltaTime
			@remotePlayer.y += (@networkManager.remoteY - @remotePlayer.y) * @interpolationMultiplier * deltaTime
			@remotePlayer\setRotation @remotePlayer.rotation - MathHelper\DifferenceBetweenAngles(@networkManager.remoteRotation, @remotePlayer.rotation) * @interpolationMultiplier * deltaTime

		@player\update deltaTime

		if @player.x - @player.width / 2 > @worldWidth
			@player.x = -@player.width / 2
		elseif @player.x + @player.width / 2 < 0
			@player.x = @worldWidth + @player.width / 2


		if @player.y > @camera.height - 10 
			@player\blow!
			@player.y = 100

		@camera\lookAtObject @player
		@camera.y = 0

		@camera.x = math.min @camera.x, @worldWidth - @camera.width
		@camera.x = math.max @camera.x, 0

		@background\move @camera.x