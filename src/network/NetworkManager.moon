require "enet"

export class NetworkManager
	@isActive: false
	@isHost: false
	@isConnected: false

	@remoteX: 0
	@remoteY: 0
	@remoteRotation: 0
	@remoteName: ""

	connect: (host, port) =>
		return if @isActive 
		hostName = "#{host}:#{tostring port}"	
		print "Connecting to " .. hostName

		@host = enet.host_create!
		@server = @host\connect hostName

		@isHost = false
		@isActive = true
		@isConnected = false

	hostServer: =>
		return if @isActive 
		print "Starting server..."

		@host = enet.host_create "localhost:7778"

		@isHost = true
		@isActive = true
		@isConnected = false

	onReceive: (peer, data) =>
		return unless data
		vars = StringHelper\Split data, "|"
		return unless vars
		if #vars > 0
			switch vars[1]
				when "POS"
					@remoteX = tonumber vars[2]
					@remoteY = tonumber vars[3]
					@remoteRotation = tonumber vars[4]
				when "NAME"
					@remoteName = tostring vars[2]

	onConnect: (peer) =>
		print "CONNECTED"
		if @isHost
			@client = peer
		@isConnected = true

		sendTo = @server
		if @isHost
			sendTo = @client
		sendTo\send "NAME|" .. ConfigManager.multiplayer.name

	onDisconnect: (peer) =>
		print "DISCONNECTED"
		@isConnected = false

	update: (x, y, rotation) =>
		return unless @host
		return unless @isActive
		event = @host\service!
		return unless event

		-- New event
		switch event.type
			when "receive"
				@onReceive event.peer, event.data
			when "connect"
				@onConnect event.peer
			when "disconnect"
				@onDisconnect event.peer

		if @isConnected
			sendTo = @server
			if @isHost
				sendTo = @client
			
			sendTo\send "POS|" .. x .. "|" .. y .. "|" .. rotation