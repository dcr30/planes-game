export class ConfigManager
	@multiplayer: {}
	@ReadConfig: =>
		unless love.filesystem.isFused!
			return false

		gameRoot = love.filesystem.getSourceBaseDirectory!
		unless love.filesystem.mount gameRoot, "gameRoot"
			print "Error mounting game root directory"
			return false

		if love.filesystem.exists "gameRoot/settings/multiplayer.lua"
			print "Reading multiplayer config"
			chunk = love.filesystem.load "gameRoot/settings/multiplayer.lua"
			@multiplayer = chunk!
		print "Config reading finished"