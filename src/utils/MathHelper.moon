export class MathHelper
	@WrapAngle: (value) =>
		if not value 
			return 0
		value = math.mod(value, 360)
		if value < 0
			value += 360
		return value

	@DifferenceBetweenAngles: (firstAngle, secondAngle) =>
		difference = secondAngle - firstAngle
		while difference < -180
			difference += 360
		while difference > 180
			difference -= 360
		return difference