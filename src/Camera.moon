export class Camera
	x: 0
	y: 0
	scale: 1.5
	rotation: 0

	new: =>
		@width, @height = love.window.getDimensions!
		@width /= @scale
		@height /= @scale

	set: =>
		love.graphics.push!
		love.graphics.rotate -@rotation
		love.graphics.scale @scale, @scale
		love.graphics.translate -@x, -@y

	unset: => 
		love.graphics.pop!

	move: (dx, dy) =>
		@x += x
		@y += y

	lookAt: (x, y) =>
		@x = x - @width / 2
		@y = y - @height / 2

	lookAtObject: (object) =>
		@lookAt object.x, object.y

	setPosition:(x, y) =>
		@x = x 
		@y = y