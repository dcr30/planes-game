export class Player extends Sprite
	-- Fields

	-- rotation
	rotation: 0
	rotationSpeed: 360 / 3
	rotationLimit: 60
	rotationLimitMul: 2.5 -- >2
	-- power
	power: 1
	powerSpeed: 0.5
	-- speed
	speed: 250
	gravity: 400

	-- Methods
	new: =>
		-- Load texture
		@texture = love.graphics.newImage "assets/plane.png"
		@texture\setFilter "nearest", "nearest"

		-- Initalize width and height variables
		@width = @texture\getWidth!
		@height = @texture\getHeight!

		-- Origin at center
		@originX = @texture\getWidth! / 2
		@originY = @texture\getHeight! / 2

		-- Smoke
		@smokeParticles = ParticleEmitter 0, 0, "smoke", 6
		-- Explosion 
		@explosionParticles = ParticleEmitter 0, 0, "explosion"

	draw: =>
		-- Draw smoke
		@smokeParticles\draw!
		@explosionParticles\draw!
		-- Draw texture
		love.graphics.setColor 255, 255, 255, 255
		rotationRad = @rotation / 180 * math.pi
		love.graphics.draw @texture, @x, @y, rotationRad, @scale, @scale, @originX, @originY

	update: (deltaTime) =>
		if @rotation >= 270 - @rotationLimit and @rotation <= 270 + @rotationLimit
			rotationDiff = 1 - math.abs(270 - @rotation) / @rotationLimit
			@setPower @power - @powerSpeed * @rotationLimitMul * rotationDiff * deltaTime

		-- Movement
		rotationRad = @rotation / 180 * math.pi
		moveX = math.cos rotationRad
		moveY = math.sin rotationRad
		-- Result speed
		moveSpeed = @speed * @power
		-- Gravity
		gravitySpeed = moveY + @gravity * (1 - @power)

		@x += moveX * moveSpeed * deltaTime
		@y += (moveY * moveSpeed + gravitySpeed) * deltaTime

		@smokeParticles.x = @x
		@smokeParticles.y = @y
		@smokeParticles\update deltaTime

		@explosionParticles\update deltaTime

	setPower: (power) =>
		if power < 0 
			power = 0
		elseif power > 1
			power = 1
		@power = power

	blow: =>
		@explosionParticles\start @x, @y

	setRotation: (rotation) =>
		@rotation = MathHelper\WrapAngle rotation